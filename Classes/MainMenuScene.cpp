#include "MainMenuScene.h"

static void LoginFacebook()
{
#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) 
	JniMethodInfo methodInfo;

	if (!cocos2d::JniHelper::getStaticMethodInfo(methodInfo, "org/cocos2dx/cpp/AppActivity", "Loginfacebook", "()V"))
	{
		return;
	}

	methodInfo.env->CallStaticVoidMethod(methodInfo.classID, methodInfo.methodID);
	methodInfo.env->DeleteLocalRef(methodInfo.classID);
#endif
}

Scene* MainMenuScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = MainMenuScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool MainMenuScene::init()
{
	if (!LayerColor::initWithColor(Color4B(60,156,178,255)))
	{
		return false;
	}

	// Background Music
	SoundManager::getInstance()->playBackgroundMusic(true);

	// m_Title
	m_Title = Sprite::createWithSpriteFrameName("logo_home.png");
	m_Title->setRotation(-5);
	m_Title->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width / 2 + 30, ORIGIN.y + VISIBLESIZE.height - m_Title->getContentSize().height / 2 - 10));
	m_Title->runAction(RepeatForever::create(Sequence::create(RotateTo::create(1, 5), RotateTo::create(1, -5), nullptr)));
	this->addChild(m_Title);

	// Plus Button
	m_plusButton = ui::Button::create("plus.png", "", "", ui::Widget::TextureResType::PLIST);
	m_plusButton->setPosition(Vec2(ORIGIN.x - VISIBLESIZE.width, ORIGIN.y + VISIBLESIZE.height / 2 - m_plusButton->getContentSize().height / 4 + 20));
	m_plusButton->setScale(0.75);
	m_plusButton->runAction(MoveTo::create(0.25, Vec2(ORIGIN.x + VISIBLESIZE.width / 4 + 20, m_plusButton->getPositionY())));
	m_plusButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){

		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::ENDED:
			gotoGamePlayScene(GameState::PLUS);
			break;
		default:
			break;
		}

	});
	this->addChild(m_plusButton);

	// Multiply Button
	m_multiplyButton = ui::Button::create("multiply.png", "", "", ui::Widget::TextureResType::PLIST);
	m_multiplyButton->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width * 3 / 2, ORIGIN.y + VISIBLESIZE.height / 2 - m_multiplyButton->getContentSize().height));
	m_multiplyButton->setScale(0.75);
	m_multiplyButton->runAction(MoveTo::create(0.25, Vec2(ORIGIN.x + VISIBLESIZE.width / 4 + 20, m_multiplyButton->getPositionY())));
	m_multiplyButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){

		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::ENDED:
			gotoGamePlayScene(GameState::MULTIPLY);
			break;
		default:
			break;
		}

	});
	this->addChild(m_multiplyButton);

	// Minus Button
	m_minusButton = ui::Button::create("minus.png", "", "", ui::Widget::TextureResType::PLIST);
	m_minusButton->setPosition(Vec2(ORIGIN.x - VISIBLESIZE.width / 2, ORIGIN.y + VISIBLESIZE.height / 2 - m_minusButton->getContentSize().height / 4 + 20));
	m_minusButton->setScale(0.75);
	m_minusButton->runAction(MoveTo::create(0.25, Vec2(ORIGIN.x + VISIBLESIZE.width * 3 / 4 - 20, m_minusButton->getPositionY())));
	m_minusButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){
		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::ENDED:
			gotoGamePlayScene(GameState::MINUS);
			break;
		default:
			break;
		}
	});
	this->addChild(m_minusButton);

	// Devide Button
	m_devideButton = ui::Button::create("devide.png", "", "", ui::Widget::TextureResType::PLIST);
	m_devideButton->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width * 2, ORIGIN.y + VISIBLESIZE.height / 2 - m_devideButton->getContentSize().height));
	m_devideButton->setScale(0.75);
	m_devideButton->runAction(MoveTo::create(0.25, Vec2(ORIGIN.x + VISIBLESIZE.width * 3 / 4 - 20, m_devideButton->getPositionY())));
	m_devideButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){

		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::ENDED:
			gotoGamePlayScene(GameState::DIVIDE);
			break;
		default:
			break;
		}

	});
	this->addChild(m_devideButton);

	// Mix Button
	m_mixButton = ui::Button::create("mix.png", "", "", ui::Widget::TextureResType::PLIST);
	m_mixButton->setPosition(Vec2(ORIGIN.x - VISIBLESIZE.width * 3 / 4, ORIGIN.y + VISIBLESIZE.height / 2 - m_mixButton->getContentSize().height * 2));
	m_mixButton->setScale(0.75);
	m_mixButton->runAction(MoveTo::create(0.25, Vec2(ORIGIN.x + VISIBLESIZE.width / 2, m_mixButton->getPositionY())));
	m_mixButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){

		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::ENDED:
			gotoGamePlayScene(GameState::MIX);
			break;
		default:
			break;
		}

	});
	this->addChild(m_mixButton);

	// Facebook Share Button
	m_facebookButton = ui::Button::create("facebookButton.png", "", "", ui::Widget::TextureResType::PLIST);
	m_facebookButton->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width - m_facebookButton->getContentSize().width, ORIGIN.y + m_facebookButton->getContentSize().height));
	m_facebookButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){

		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::ENDED:
			SoundManager::getInstance()->playEffectButton();
			LoginFacebook();
			break;
		default:
			break;
		}

	});
	this->addChild(m_facebookButton);

	// Dev info
	m_devInfo = Label::createWithTTF("# Thanh Nguyen 2017", FONT, 18);
	m_devInfo->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width / 2, ORIGIN.y + 12));
	this->addChild(m_devInfo);

	return true;
}

void MainMenuScene::gotoGamePlayScene(GameState _state)
{
	// EFFECT
	SoundManager::getInstance()->playEffectButton();

	// SET GAME STATE
	GameManager::getInstance()->setGameState(_state);

	// REPLACE SCENE
	SceneManager::getInstance()->MainMenuSceneToGamePlayScene();
}
