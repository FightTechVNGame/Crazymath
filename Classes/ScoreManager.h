#ifndef _SCORE_MANAGER_H__
#define _SCORE_MANAGER_H__

#include "cocos2d.h"
USING_NS_CC;

class ScoreManager
{
public:
	~ScoreManager();

	static ScoreManager* getInstance();

	void setHighScore(const char* key, int _score);
	int getHighScore(const char* key);

private:
	ScoreManager();
	static ScoreManager* m_instance;
	UserDefault* m_userDefault;

	int m_highScore;
};


#endif //_SCORE_MANAGER_H__