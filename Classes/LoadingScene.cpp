﻿#include "LoadingScene.h"

Scene* LoadingScene::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = LoadingScene::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool LoadingScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	m_loadingStep = 0;
	m_totalTime = 0;
	m_counter = 0;
	m_isDone = false;

	// Title
	m_title = Sprite::create("HelloWorld.png");
	m_title->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width / 2, ORIGIN.y + VISIBLESIZE.height * 2 / 3));
	this->addChild(m_title);

	// Countdown Label
	m_countdownLabel = Label::createWithTTF("0%%", FONT, 30);
	m_countdownLabel->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width / 2, ORIGIN.y + VISIBLESIZE.height - m_title->getPositionY()));
	this->addChild(m_countdownLabel);

	// Loading Bar BG
	m_loadingBarBG = Sprite::create("ic_loading.png");
	m_loadingBarBG->setPosition(Point(ORIGIN.x + VISIBLESIZE.width / 2.0f, ORIGIN.y + VISIBLESIZE.height / 4.0f + m_loadingBarBG->getContentSize().height / 4.0f));
	m_loadingBarBG->setColor(Color3B(41, 201, 226));
	m_loadingBarBG->setScaleX(0.5);
	this->addChild(m_loadingBarBG);

	// Loading
	m_loadingBar = ui::LoadingBar::create();
	m_loadingBar->loadTexture("ic_loading.png");
	m_loadingBar->setPercent(0);
	m_loadingBar->setPosition(Point(ORIGIN.x + VISIBLESIZE.width / 2.0f, ORIGIN.y + VISIBLESIZE.height / 4.0f + m_loadingBar->getContentSize().height / 4.0f));
	m_loadingBar->setScaleX(0.5);
	this->addChild(m_loadingBar);

	this->scheduleUpdate();

	return true;
}

void LoadingScene::update(float dt)
{
	m_counter++;

	if (m_counter <= 100)
	{
		m_countdownLabel->setString(String::createWithFormat("%d%%", m_counter)->getCString());
		m_loadingBar->setPercent(m_counter);
	}

	m_totalTime += dt;

	switch (m_loadingStep)
	{
	case 0:
		break;
	case 1:
		// GetData from Server
		break;
	case 2:
		// Get GameInfo JSON
		break;
	case 3:
	{
			  // Load lên Ram trước nên đỡ tốn chi phí
			  spritecache = SpriteFrameCache::getInstance();
			  spritecache->addSpriteFramesWithFile("asset.plist");
				
			  // Background Music
			  SoundManager::getInstance()->preloadBackgroundMusic(BACKGROUND_MUSIC);

			  // EFFECT
			  SoundManager::getInstance()->preloadEffect(SOUND_SCORED);
			  SoundManager::getInstance()->preloadEffect(SOUND_FAILED);
			  SoundManager::getInstance()->preloadEffect(SOUND_BUTTON_CLICK);

			  m_isDone = true;
			  break;
	}
	default:
		break;
	}

	m_loadingStep++;

	if (m_isDone && (int)(m_totalTime) >= 3)
	{
		SceneManager::getInstance()->LoadingSceneToMainMenuScene();
	}
}
