#ifndef _OVER_LAYER_H__
#define _OVER_LAYER_H__

#include "Definitions.h"

class OverLayer : public cocos2d::Layer
{
public:
	virtual bool init();
	CREATE_FUNC(OverLayer);

	void setScore(int _score);
private:
	int m_highscore;

	Label* m_scoreLabel;
	Label* m_highscoreLabel;

	Sprite* m_backgroundLayer;
	ui::Button* m_replayButton;
	ui::Button* m_menuButton;
	
	void CheckHighScore(const char* key, int _score);

	void gotoMainMenuScene();
	void gotoGamePlayScene();

};

#endif //_OVER_LAYER_H__