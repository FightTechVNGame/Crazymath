#ifndef _SOUND_MANAGER_H__
#define _SOUND_MANAGER_H__

#include "Definitions.h"
#include "SimpleAudioEngine.h"
using namespace CocosDenshion;

class SoundManager
{
public:
	~SoundManager();
	static SoundManager* getInstance();

	void preloadBackgroundMusic(const char* filepath);
	void preloadEffect(const char* filePath);

	void playBackgroundMusic(bool _loop = false);
	void playEffectButton(bool loop = false);
	void playEffectScore(bool loop = false);
	void playEffectFailed(bool loop = false);

	void Stop();
	void Play();

	inline bool getSound() { return m_soundOn; }
private:
	SoundManager();
	static SoundManager* m_instance;

	SimpleAudioEngine* m_audio;

	bool m_soundOn;
};


#endif //