#ifndef __GAMEPLAY_SCENE_H__
#define __GAMEPLAY_SCENE_H__

#include "MathLayer.h"
#include "Definitions.h"

class GamePlayScene : public cocos2d::LayerColor
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
	CREATE_FUNC(GamePlayScene);
private:

};

#endif // __GAMEPLAY_SCENE_H__
