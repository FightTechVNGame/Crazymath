#include "MathLayer.h"

bool MathLayer::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// init variables
	m_time = TIME_BEGINNER;
	m_currentTimeLevel = m_time;
	m_num1 = 0;
	m_num2 = 0;
	m_score = 0;
	m_check = -1;
	m_mathStr = "";
	m_soundOn = SoundManager::getInstance()->getSound();
	m_state = GameManager::getInstance()->getGameState();

	// True + False button
	m_trueBtn = ui::Button::create("true.png", "true_select.png", "", ui::Widget::TextureResType::PLIST);
	m_trueBtn->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width / 4, ORIGIN.y + m_trueBtn->getContentSize().height / 2));
	m_trueBtn->setScale(0.8);
	m_trueBtn->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){

		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::ENDED:
			trueButtonClick();
			break;
		default:
			break;
		}

	});
	this->addChild(m_trueBtn);

	m_falseBtn = ui::Button::create("false.png", "false_select.png", "", ui::Widget::TextureResType::PLIST);
	m_falseBtn->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width * 3 / 4, ORIGIN.y + m_falseBtn->getContentSize().height / 2));
	m_falseBtn->setScale(0.8);
	m_falseBtn->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){

		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::ENDED:
			falseButtonClick();
			break;
		default:
			break;
		}

	});
	this->addChild(m_falseBtn);

	// Math Label
	m_math = Label::createWithTTF(m_mathStr, FONT, 120);
	m_math->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width / 2, ORIGIN.y + (VISIBLESIZE.height / 3) * 2));
	m_math->setAlignment(TextHAlignment::CENTER);
	m_math->setLineHeight(100);
	this->addChild(m_math);

	// Loading Bar
	m_loadingBar = ui::LoadingBar::create();
	m_loadingBar->loadTexture("ic_loading.png");
	m_loadingBar->setPercent(100);
	m_loadingBar->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width / 2, ORIGIN.y + VISIBLESIZE.height - (m_loadingBar->getContentSize().height / 2)));
	this->addChild(m_loadingBar);

	// SCORE LABEL
	m_scoreLabel = Label::createWithTTF("0", FONT, 50);
	m_scoreLabel->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width - 50, ORIGIN.y + VISIBLESIZE.height - 50));
	this->addChild(m_scoreLabel);

	// SOUND BUTTON
	m_soundButton = ui::Button::create();

	if (m_soundOn)
	{
		m_soundButton->loadTextures("sound_on.png", "", "", ui::Widget::TextureResType::PLIST);
	}
	else
	{
		m_soundButton->loadTextures("sound_off.png", "", "", ui::Widget::TextureResType::PLIST);
	}

	m_soundButton->setScale(0.8);
	m_soundButton->setPosition(Vec2(ORIGIN.x + m_soundButton->getContentSize().width, ORIGIN.y + VISIBLESIZE.height - m_soundButton->getContentSize().height));
	m_soundButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){

		if (m_soundOn)
		{
			switch (type)
			{
			case cocos2d::ui::Widget::TouchEventType::BEGAN:
			{
				m_soundOn = false;
				SoundManager::getInstance()->Stop();
				m_soundButton->loadTextures("sound_off.png", "", "", ui::Widget::TextureResType::PLIST);
				break;
			}
			default:
				break;
			}
		}
		else
		{
			switch (type)
			{
			case cocos2d::ui::Widget::TouchEventType::BEGAN:
			{
				m_soundOn = true;
				SoundManager::getInstance()->Play();
				m_soundButton->loadTextures("sound_on.png", "", "", ui::Widget::TextureResType::PLIST);
				break;
			}
			default:
				break;
			}
		}
	});


	this->addChild(m_soundButton);

	// RANDOM MATH
	randomMath(m_state);

	this->scheduleUpdate();

	return true;
}

void MathLayer::update(float dt)
{
	if (m_score == 0)
		return;

	m_time -= dt;
	m_loadingBar->setPercent(m_time * (100 / m_currentTimeLevel));

	if (m_time < 0)
	{
		m_time = 0;

		// Show over layer
		SoundManager::getInstance()->playEffectFailed();
		showOverLayer();
	}
}

void MathLayer::setTime()
{
	if (m_score <= 5)
	{
		m_time = TIME_BEGINNER;
		m_currentTimeLevel = m_time;
	}

	if (m_score >= 6 && m_score <= 10)
	{
		m_time =TIME_EASY;
		m_currentTimeLevel = m_time;
	}

	if (m_score >= 11 && m_score <= 15)
	{
		m_time =TIME_MEDIUM;
		m_currentTimeLevel = m_time;
	}

	if (m_score > 15)
	{
		m_time = TIME_HARD;
		m_currentTimeLevel = m_time;
	}
}

void MathLayer::randomMath(GameState _state)
{
	switch (_state)
	{
	case GameState::PLUS:
	{
			  Plus();
			  break;
	}
	case GameState::MINUS:
	{
			  Minus();
			  break;
	}
	case GameState::MULTIPLY:
	{
			  Multiply();
			  break;
	}
	case GameState::DIVIDE:
	{
			  Divide();
			  break;
	}
	case GameState::MIX:
	{
			  Mix();
			  break;
	}
	default:
		break;
	}
}

void MathLayer::Plus()
{
	m_num1 = random(1, m_score + 10); // increases with m_score
	m_num2 = random(1, m_score + 10);
	m_check = random(0, 1); // result

	switch (m_check)
	{
	case 0: // wrong result
	{
				m_mathStr = String::createWithFormat("%d + %d \n= %d", m_num1, m_num2, random(m_num1, m_num1 + m_num2 - 1))->getCString();
				break;
	}
	case 1: // correct result
	{
				m_mathStr = String::createWithFormat("%d + %d \n= %d", m_num1, m_num2, m_num1 + m_num2)->getCString();
				break;
	}
	default:
		break;
	}

	m_math->setString(m_mathStr);

	m_num1 = 0;
	m_num2 = 0;
}

void MathLayer::Minus()
{
	m_num1 = random(1, m_score + 10); // increases with m_score
	m_num2 = random(1, m_score + 10);
	m_check = random(0, 1); // result

	switch (m_check)
	{
	case 0: // wrong result
	{
				m_mathStr = String::createWithFormat("%d - %d \n= %d", m_num1, m_num2, m_num1 - m_num2 + random(1, 5))->getCString();
				break;
	}
	case 1: // correct result
	{
				m_mathStr = String::createWithFormat("%d - %d \n= %d", m_num1, m_num2, m_num1 - m_num2)->getCString();
				break;
	}
	default:
		break;
	}

	m_math->setString(m_mathStr);

	m_num1 = 0;
	m_num2 = 0;
}

void MathLayer::Multiply()
{
	int m_num1 = random(1, m_score + 10); // increases with m_score
	int m_num2 = random(1, m_score + 10);
	m_check = random(0, 1); // result

	switch (m_check)
	{
	case 0: // wrong result
	{
				m_mathStr = String::createWithFormat("%d x %d \n= %d", m_num1, m_num2, m_num1 * m_num2 - random(1, 5))->getCString();
				break;
	}
	case 1: // correct result
	{
				m_mathStr = String::createWithFormat("%d x %d \n= %d", m_num1, m_num2, m_num1 * m_num2)->getCString();
				break;
	}
	default:
		break;
	}

	m_math->setString(m_mathStr);

	m_num1 = 0;
	m_num2 = 0;
}

void MathLayer::Divide()
{
	m_num1 = random(1, m_score + 10); // increases with m_score
	m_num2 = random(1, m_score + 10);
	m_check = random(0, 1); // result

	switch (m_check)
	{
	case 0: // wrong result
	{
				m_mathStr = String::createWithFormat("%d : %d \n= %.2f", m_num1, m_num2, (float)m_num1 / m_num2 + (float)random(1, 5))->getCString();
				break;
	}
	case 1: // correct result
	{
				m_mathStr = String::createWithFormat("%d : %d \n= %.2f", m_num1, m_num2, (float)m_num1 / m_num2)->getCString();
				break;
	}
	default:
		break;
	}

	m_math->setString(m_mathStr);

	m_num1 = 0;
	m_num2 = 0;
}

void MathLayer::Mix()
{
	int _operator = random(1, 4); // random operator

	switch (_operator)
	{
	case 1: // Plus
	{
				Plus();
				break;
	}
	case 2: // Minus
	{
				Minus();
				break;
	}
	case 3: // Multiply
	{
				Multiply();
				break;
	}
	case 4: // Divide
	{
				Divide();
				break;
	}
	default:
		break;
	}
}

void MathLayer::trueButtonClick()
{
	if (m_check == 1)
	{
		// Effect
		SoundManager::getInstance()->playEffectScore();

		setTime();

		randomMath(m_state);

		m_score++;
		m_scoreLabel->setString(String::createWithFormat("%d", m_score)->getCString());
	}
	else
	{
		// Effect
		SoundManager::getInstance()->playEffectFailed();

		// Show over layer
		showOverLayer();
	}
}

void MathLayer::falseButtonClick()
{
	if (m_check == 0)
	{
		// Effect
		SoundManager::getInstance()->playEffectScore();

		setTime();

		randomMath(m_state);

		m_score++;
		m_scoreLabel->setString(String::createWithFormat("%d", m_score)->getCString());
	}
	else
	{
		SoundManager::getInstance()->playEffectFailed();

		// Show over layer
		showOverLayer();
	}
}

void MathLayer::showOverLayer()
{
	this->pause();

	// Disable Buttons
	m_trueBtn->setTouchEnabled(false);
	m_falseBtn->setTouchEnabled(false);
	m_soundButton->setTouchEnabled(false);

	// Remove Score Label
	m_scoreLabel->removeFromParentAndCleanup(true);

	// REMOVE MATH LABEL
	m_math->removeFromParentAndCleanup(true);

	// OverLayer
	OverLayer* overlayer = OverLayer::create();
	overlayer->setScore(m_score);
	overlayer->setPosition(Vec2(0, ORIGIN.y + VISIBLESIZE.height));
	MoveTo* action = MoveTo::create(0.25, Point(0, 0));
	overlayer->runAction(action);

	this->addChild(overlayer);
}