#ifndef _DEFINITIONS_H__
#define _DEFINITIONS_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "GameManager.h"
#include "SoundManager.h"
#include "ScoreManager.h"
#include "SceneManager.h"

USING_NS_CC;

namespace TextKeys
{
	static const char* TimeBeginner = "TimeBeginner";
	static const char* TimeEasy = "TimeEasy";
	static const char* TimeMedium = "TimeMedium";
	static const char* TimeHard = "TimeHard";
}

namespace Keys
{
	static const char* Plus = "PLUS";
	static const char* Minus = "MINUS";
	static const char* Multiply = "MULTIPLY";
	static const char* Divide = "DIVIDE";
	static const char* Mix = "MIX";
}

// VERSION
#define GAMEVERSION "1.0.0"

// SIZE
#define VISIBLESIZE Director::getInstance()->getVisibleSize()
#define ORIGIN Director::getInstance()->getVisibleOrigin()

// FONT
#define FONT "fonts/relay-black.ttf"

// AUDIO
#define BACKGROUND_MUSIC "MiningbyMoonlight.mp3"
#define SOUND_SCORED "scored.wav"
#define SOUND_FAILED "fail.wav"
#define SOUND_BUTTON_CLICK "btnaparecer.mp3"

// JSON
#define TIME_BEGINNER GameManager::getInstance()->getFloatForKey(TextKeys::TimeBeginner)
#define TIME_EASY GameManager::getInstance()->getFloatForKey(TextKeys::TimeEasy)
#define TIME_MEDIUM GameManager::getInstance()->getFloatForKey(TextKeys::TimeMedium)
#define TIME_HARD GameManager::getInstance()->getFloatForKey(TextKeys::TimeHard)

#endif //_DEFINITIONS_H__