﻿#include "GamePlayScene.h"

Scene* GamePlayScene::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
	auto layer = GamePlayScene::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool GamePlayScene::init()
{
	if (!LayerColor::initWithColor(Color4B(Color4B(random(1, 245), random(1, 245), random(1, 245), 255))))
	{
		return false;
	}

	MathLayer* mathLayer = MathLayer::create();
	this->addChild(mathLayer);

	return true;
}

