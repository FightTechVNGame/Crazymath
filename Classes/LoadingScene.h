#ifndef _LOADING_SCENE_H__
#define _LOADING_SCENE_H__

#include "Definitions.h"

class LoadingScene : public cocos2d::Layer
{
public:
	static Scene* createScene();
	virtual bool init();
	CREATE_FUNC(LoadingScene);

private:
	int m_counter;
	int m_loadingStep;
	float m_totalTime;
	bool m_isDone;

	Sprite* m_title;
	Sprite* m_loadingBarBG;
	Label* m_countdownLabel;
	ui::LoadingBar* m_loadingBar;

	// PRELOAD
	SpriteFrameCache* spritecache;

	void update(float dt);
};

#endif //_LOADING_SCENE_H__