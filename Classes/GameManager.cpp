﻿#include "GameManager.h"

GameManager* GameManager::m_instance = nullptr;

GameManager* GameManager::getInstance()
{
	if (m_instance == nullptr)
	{
		m_instance = new GameManager();
	}

	return m_instance;
}

GameManager::GameManager()
{
	string fileName = "Config.json";

	// Load file json lên
	string str = FileUtils::getInstance()->getStringFromFile(fileName);

	// Đổi chuỗi str về dạng json
	document.Parse<0>(str.c_str());

	if (document.HasParseError())
	{
		return;
	}
}

GameManager::~GameManager()
{
}

float GameManager::getFloatForKey(string key)
{
	if (document.HasParseError() == false)
	{
		// Nếu có đối tượng key
		if (document.HasMember(key.c_str()))
		{
			return  document[key.c_str()].GetDouble();
		}

		return -1.0f;
	}
}