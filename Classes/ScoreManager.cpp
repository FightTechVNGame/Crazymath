#include "ScoreManager.h"

ScoreManager* ScoreManager::m_instance = nullptr;

ScoreManager* ScoreManager::getInstance()
{
	if (m_instance == nullptr)
	{
		m_instance = new ScoreManager();
	}

	return m_instance;
}

ScoreManager::ScoreManager()
{
	m_userDefault = UserDefault::getInstance();
	//m_userDefault->deleteValueForKey("PLUS");
	//m_userDefault->deleteValueForKey("MINUS");
	//m_userDefault->deleteValueForKey("MULTIPLY");
	//m_userDefault->deleteValueForKey("DIVIDE");
	//m_userDefault->deleteValueForKey("MIX");
}

ScoreManager::~ScoreManager()
{
}

void ScoreManager::setHighScore(const char* key, int _score)
{
	m_highScore = m_userDefault->getIntegerForKey(key, 0);

	if (_score > m_highScore)
	{
		m_userDefault->setIntegerForKey(key, _score);
	}

	// SAVE
	m_userDefault->flush();
}

int ScoreManager::getHighScore(const char* key)
{
	return m_userDefault->getIntegerForKey(key, 0);
}