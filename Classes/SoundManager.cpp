#include "SoundManager.h"

SoundManager* SoundManager::m_instance = nullptr;

SoundManager* SoundManager::getInstance()
{
	if (m_instance == nullptr)
	{
		m_instance = new SoundManager();
	}

	return m_instance;
}

SoundManager::SoundManager()
{
	m_soundOn = true;
	this->m_audio = SimpleAudioEngine::getInstance();
}

SoundManager::~SoundManager()
{
}

void SoundManager::preloadBackgroundMusic(const char* filePath)
{
	this->m_audio->preloadBackgroundMusic(filePath);
}

void SoundManager::preloadEffect(const char* filePath)
{
	this->m_audio->preloadEffect(filePath);
}

void SoundManager::playBackgroundMusic(bool _loop)
{
	if (!m_soundOn)
		return;
	else
		this->m_audio->playBackgroundMusic(BACKGROUND_MUSIC, _loop);
}

void SoundManager::playEffectButton(bool _loop)
{
	this->m_audio->playEffect(SOUND_BUTTON_CLICK);
}

void SoundManager::playEffectScore(bool _loop)
{
	this->m_audio->playEffect(SOUND_SCORED);
}

void SoundManager::playEffectFailed(bool _loop)
{
	this->m_audio->playEffect(SOUND_FAILED);
}

void SoundManager::Stop()
{
	this->m_soundOn = false;
	this->m_audio->stopBackgroundMusic();
}

void SoundManager::Play()
{
	this->m_soundOn = true;
	this->m_audio->resumeBackgroundMusic();
}





