#ifndef _SCENE_MANAGER_H__
#define _SCENE_MANAGER_H__

#include "MainMenuScene.h"
#include "GamePlayScene.h"

class SceneManager
{
public:
	~SceneManager();
	static SceneManager* getInstance();

	void LoadingSceneToMainMenuScene();
	void MainMenuSceneToGamePlayScene();
	void GamePlaySceneToGamePlayScene();
	void GamePlaySceneToMainMenuScene();
private:
	SceneManager();
	static SceneManager* m_instance;
};

#endif //_SCENE_MANAGER_H__