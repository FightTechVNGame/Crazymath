#include "OverLayer.h"

bool OverLayer::init()
{
	if (!Layer::init())
	{
		return false;
	}

	// Background
	m_backgroundLayer = Sprite::createWithSpriteFrameName("game_over_bg.png");
	m_backgroundLayer->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width / 2, ORIGIN.y + VISIBLESIZE.height * 2 / 3));
	m_backgroundLayer->setScale(0.75);
	this->addChild(m_backgroundLayer);

	// Replay Button
	m_replayButton = ui::Button::create("play.png", "play_select.png", "", ui::Widget::TextureResType::PLIST);
	m_replayButton->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width / 2 - m_replayButton->getContentSize().width / 3, ORIGIN.y + m_backgroundLayer->getPositionY() - m_backgroundLayer->getContentSize().height / 2));
	m_replayButton->setScale(0.6);
	m_replayButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){
	
		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::ENDED:
			gotoGamePlayScene();
			break;
		default:
			break;
		}

	});
	this->addChild(m_replayButton);

	// Menu Button
	m_menuButton = ui::Button::create("menu.png", "menu_select.png", "", ui::Widget::TextureResType::PLIST);
	m_menuButton->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width / 2 + m_menuButton->getContentSize().width / 3, ORIGIN.y + m_backgroundLayer->getPositionY() - m_backgroundLayer->getContentSize().height / 2));
	m_menuButton->setScale(0.6);
	m_menuButton->addTouchEventListener([&](Ref* sender, ui::Widget::TouchEventType type){

		switch (type)
		{
		case cocos2d::ui::Widget::TouchEventType::ENDED:
			gotoMainMenuScene();
			break;
		default:
			break;
		}

	});
	this->addChild(m_menuButton);

	// Score Label
	m_scoreLabel = Label::createWithTTF("0", FONT, 50);
	m_scoreLabel->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width / 2 + 30, ORIGIN.y + m_backgroundLayer->getPositionY() + 20));
	this->addChild(m_scoreLabel);

	// High Score Label
	m_highscoreLabel = Label::createWithTTF("0", FONT, 50);
	m_highscoreLabel->setColor(Color3B::YELLOW);
	m_highscoreLabel->setPosition(Vec2(ORIGIN.x + VISIBLESIZE.width / 2 + 30, ORIGIN.y + m_backgroundLayer->getPositionY() - 60));
	this->addChild(m_highscoreLabel);

	return true;
}

void OverLayer::gotoGamePlayScene()
{
	SoundManager::getInstance()->playEffectButton();
	SceneManager::getInstance()->GamePlaySceneToGamePlayScene();
}

void OverLayer::gotoMainMenuScene()
{
	SoundManager::getInstance()->playEffectButton();
	SceneManager::getInstance()->GamePlaySceneToMainMenuScene();
}

void OverLayer::setScore(int _score)
{
	switch (GameManager::getInstance()->getGameState())
	{
	case GameState::PLUS:
	{
		CheckHighScore(Keys::Plus, _score);
		break;
	}
	case GameState::MINUS:
	{
		CheckHighScore(Keys::Minus, _score);
		break;
	}
	case GameState::MULTIPLY:
	{
		CheckHighScore(Keys::Multiply, _score);
		break;
	}
	case GameState::DIVIDE:
	{
		CheckHighScore(Keys::Divide, _score);
		break;
	}
	case GameState::MIX:
	{
		CheckHighScore(Keys::Mix, _score);
		break;
	}
	default:
		break;
	}
}

void OverLayer::CheckHighScore(const char* key, int _score)
{
	// Check high score
	ScoreManager::getInstance()->setHighScore(key, _score);

	// Get back high score
	m_highscore = ScoreManager::getInstance()->getHighScore(key);

	m_scoreLabel->setString(String::createWithFormat("%d", _score)->getCString());
	m_highscoreLabel->setString(String::createWithFormat("%d", m_highscore)->getCString());
}
