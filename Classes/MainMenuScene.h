#ifndef _MAINMENU_SCENE_H__
#define _MAINMENU_SCENE_H__

#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID) 
#include "platform/android/jni/JniHelper.h"
#include <jni.h> 
#endif

#include "Definitions.h"

class MainMenuScene : public cocos2d::LayerColor
{
public:
	static Scene* createScene();
	virtual bool init();
	CREATE_FUNC(MainMenuScene);
private:
	Sprite* m_Title;
	Label* m_devInfo;

	// 1 : Plus , 2 : Minus , 3 : Multiply , 4 : Divide , 5 : Mix
	ui::Button* m_plusButton;
	ui::Button* m_minusButton;
	ui::Button* m_multiplyButton;
	ui::Button* m_devideButton;
	ui::Button* m_mixButton;
	ui::Button* m_facebookButton;

	void gotoGamePlayScene(GameState _state);
};

#endif //_MAINMENU_SCENE_H__