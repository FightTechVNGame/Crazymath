#ifndef _MATH_LAYER_H__
#define _MATH_LAYER_H__

#include "Definitions.h"
#include "OverLayer.h"

class MathLayer : public Layer
{
public:
	virtual bool init();
	CREATE_FUNC(MathLayer);
private:
	std::string m_mathStr;
	int m_num1, m_num2;
	int m_score;
	int m_check;
	float m_time;
	float m_currentTimeLevel;
	bool m_soundOn;
	GameState m_state;

	Label* m_math;
	Label* m_scoreLabel;

	ui::Button* m_trueBtn;
	ui::Button* m_falseBtn;
	ui::Button* m_soundButton;
	ui::LoadingBar* m_loadingBar;

	void trueButtonClick();
	void falseButtonClick();

	void Plus();
	void Minus();
	void Multiply();
	void Divide();
	void Mix();

	void showOverLayer();
	void randomMath(GameState _state);

	void setTime();

	// UPDATE
	void update(float dt);
};


#endif //_MATH_LAYER_H__