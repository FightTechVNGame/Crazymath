#ifndef _GAME_MANAGER_H__
#define _GAME_MANAGER_H__

#include "cocos2d.h"
#include <json/document.h>
#include <string>

USING_NS_CC;
using namespace rapidjson;
using namespace std;

enum GameState
{
	PLUS,
	MINUS,
	MULTIPLY,
	DIVIDE,
	MIX
};

class GameManager
{
public:
	~GameManager();
	
	float getFloatForKey(string key);

	inline void setGameState(GameState _state) { m_state = _state; }
	inline GameState getGameState() { return m_state; }

	static GameManager* getInstance();
private:
	static GameManager* m_instance;
	Document document;
	GameManager();

	GameState m_state;
};



#endif //_GAME_MANAGER_H__