#include "SceneManager.h"

SceneManager* SceneManager::m_instance = nullptr;

SceneManager* SceneManager::getInstance()
{
	if (m_instance == nullptr)
	{
		m_instance = new SceneManager();
	}

	return m_instance;
}

SceneManager::SceneManager()
{
}

SceneManager::~SceneManager()
{
}

void SceneManager::LoadingSceneToMainMenuScene()
{
	Scene* mainMenu = MainMenuScene::createScene();
	Director::getInstance()->replaceScene(mainMenu);
}

void SceneManager::MainMenuSceneToGamePlayScene()
{
	Scene* gamePlay = GamePlayScene::createScene();
	Director::getInstance()->replaceScene(TransitionMoveInR::create(0.25, gamePlay));
}

void SceneManager::GamePlaySceneToGamePlayScene()
{
	Scene* gamePlay = GamePlayScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(0.25, gamePlay));
}

void SceneManager::GamePlaySceneToMainMenuScene()
{
	Scene* gamePlay = MainMenuScene::createScene();
	Director::getInstance()->replaceScene(TransitionFade::create(0.25, gamePlay));
}


