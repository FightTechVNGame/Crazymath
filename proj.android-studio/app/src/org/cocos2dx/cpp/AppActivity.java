/****************************************************************************
Copyright (c) 2015 Chukong Technologies Inc.
 
http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
****************************************************************************/
package org.cocos2dx.cpp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.cocos2dx.lib.Cocos2dxActivity;
import org.json.JSONObject;

import java.net.URL;

public class AppActivity extends Cocos2dxActivity{

    private CallbackManager callbackManager;
    private FacebookCallback<LoginResult> loginResult;

    public static AppActivity instance = null;

    URL imageURL = null;

    ShareDialog shareDialog;

    AccessToken accessToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        instance = this;

        callbackManager = CallbackManager.Factory.create();
        loginResult = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("ThanhLog","User ID: " + loginResult.getAccessToken().getUserId());
                Log.d("ThanhLog","Auth Token: " + loginResult.getAccessToken().getToken());

                GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        if(object == null)
                            return;
                        Log.d("ThanhLog",object.toString());
                        // Application code
                        String name = object.optString("name");
                        String id = object.optString("id");
                        String email = object.optString("email");
                        String link = object.optString("link");
                        imageURL = extractIcon(id);

                        Log.d("ThanhLog","name: " + name);
                        Log.d("ThanhLog","id: " + id);
                        Log.d("ThanhLog","email: " + email);
                        Log.d("ThanhLog","link: " + link);
                        Log.d("ThanhLog","imageURL: " + imageURL);
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields","id,first_name,last_name,name,email,gender,birthday,phone");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.d("ThanhLog","loginResult onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("ThanhLog","loginResult error: " + error.toString());
            }
        };


        LoginManager.getInstance().registerCallback(callbackManager,loginResult);

        shareDialog = new ShareDialog(this);
        shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Log.d("ThanhLog", "onSuccess: " + result.toString());
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Log.d("ThanhLog", error.toString());
            }
        });

        accessToken = AccessToken.getCurrentAccessToken();
    }


    public void Sharefacebook()
    {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentTitle("Crazy Math")
                    .setRef("zzzzzzzzzzzz")
                    //.setQuote("Let's play game.")
                    .setContentDescription("'Crazy Math' by dev Thanh")
                    .setShareHashtag(new ShareHashtag.Builder().setHashtag("#CrazyMath").build())
                    .setContentUrl(Uri.parse("https://play.google.com/"))
                    .setImageUrl(Uri.parse("https://lh3.googleusercontent.com/NPNmHJ3zWTE-UqyNlehgB5udeMotSyMCfDZMwTramUZ8UZARTbYYX72iOXUKod-rbYGRRw=s85"))
                    .build();

            shareDialog.show(linkContent);
        }
    }

    public static void Loginfacebook()
    {
        if (AppActivity.instance == null)
        {
            return;
        }

        AppActivity.instance.onLoginfacebook();
    }

    public void onLoginfacebook()
    {
        Sharefacebook();
        //LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));
    }

    public URL extractIcon(String id) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            URL imageURL = new URL("https://graph.facebook.com/" + id + "/picture?type=large");
            return imageURL;
        } catch (Throwable e)
        {
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
