set prjName=CrazyMath
cls

call cocos compile -p android --android-studio

pause 

cd bin\debug\android
call adb install -r %prjName%-debug.apk

call adb shell am start -n org.cocos2dx.%prjName%/org.cocos2dx.cpp.AppActivity
pause 

cls

call build_android_studio.bat